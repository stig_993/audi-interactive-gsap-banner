//Set these to the banner dimentions
var BANNER_WIDTH=728;
var BANNER_HEIGHT=90;

//This is used to report timing
var stopWatch;

//Set the initial states of all divs here
function setInitialStates(){
	hideAll([".start", ".replayBtn", ".ctaContainer"]);
}

//This gets called when the ad is finished loading
function mainInit(){
	addListeners();
	setInitialStates();

	// show the ad and start animation
	$(".container").show();
	seq01();
}

// x and y for the main copy
var copyPos = {x: 15, y: 23};

//The first function in our sequence of animations
function seq01(){
	console.log("seq01");
		
	stopWatch = new Date().getTime(); 

	var twnDelay = 0;

	TweenLite.to($('.q7'), 1, {top:20})
	TweenLite.to($('.q7Txt'), 1, {top: 1})

	twnDelay += 0.5;
	
	TweenLite.to($('.a6'), 1, {top:19, delay:twnDelay})
	TweenLite.to($('.a6Txt'), 1, {top:1, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.s8'), 1, {top:18, delay:twnDelay})
	TweenLite.to($('.s8Txt'), 1, {top:1, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.choose1'), 1, {top:5, delay:twnDelay})
	TweenLite.to($('.choose2'), 1, {top:5, delay:twnDelay})

	$('.ctaContainer').show()
	
}

function seq02(){
	console.log("seq02");

	var twnDelay=0;

	// hide seq 1
	TweenLite.to($('.q7'), 1, {autoAlpha:0})
	TweenLite.to($('.q7Txt'), 1, {autoAlpha:0})
	TweenLite.to($('.a6'), 1, {autoAlpha:0})
	TweenLite.to($('.a6Txt'), 1, {autoAlpha:0})
	TweenLite.to($('.s8'), 1, {autoAlpha:0})
	TweenLite.to($('.s8Txt'), 1, {autoAlpha:0})
	TweenLite.to($('.choose1'), 1, {autoAlpha:0})
	TweenLite.to($('.choose2'), 1, {autoAlpha:0})
	// hide seq 1

	twnDelay += 0.5;

	TweenLite.to($('.q7-2'), 1, {top:3, delay:twnDelay})
	TweenLite.to($('.q7Txt-2'), 1, {top:1, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.premium'), 1, {top:4, delay:twnDelay})
	TweenLite.to($('.premiumPrice'), 1, {top:27, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.pplus'), 1, {top:5, delay:twnDelay})
	TweenLite.to($('.ppPrice'), 1, {top:27, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.prestige'), 1, {top:34, delay:twnDelay})
	TweenLite.to($('.prestigePrice'), 1, {top:57, delay:twnDelay})

	twnDelay += 0.5;
	
	TweenLite.to($('.start'), 1, {autoAlpha:1, delay:twnDelay})
	$('.start').show();

	twnDelay += 0.5;

	TweenLite.delayedCall(twnDelay, doResolve);

}

function seq03(){
	console.log("seq03");

	var twnDelay=0;

	// hide seq 1
	TweenLite.to($('.q7'), 1, {autoAlpha:0})
	TweenLite.to($('.q7Txt'), 1, {autoAlpha:0})
	TweenLite.to($('.a6'), 1, {autoAlpha:0})
	TweenLite.to($('.a6Txt'), 1, {autoAlpha:0})
	TweenLite.to($('.s8'), 1, {autoAlpha:0})
	TweenLite.to($('.s8Txt'), 1, {autoAlpha:0})
	TweenLite.to($('.choose1'), 1, {autoAlpha:0})
	TweenLite.to($('.choose2'), 1, {autoAlpha:0})
	// hide seq 1

	twnDelay += 0.5;

	TweenLite.to($('.a6-2'), 1, {top:3, delay:twnDelay})
	TweenLite.to($('.a6Txt-2'), 1, {top:1, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.premium1'), 1, {top:4, delay:twnDelay})
	TweenLite.to($('.premiumPrice1'), 1, {top:27, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.pplus1'), 1, {top:5, delay:twnDelay})
	TweenLite.to($('.ppPrice1'), 1, {top:27, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.prestige1'), 1, {top:34, delay:twnDelay})
	TweenLite.to($('.prestigePrice1'), 1, {top:57, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.forward'), 1, {top:56})

	twnDelay += 0.5;

	$('.replayBtn').show();
	TweenLite.to($('.replayBtn img'), 1, {top: 49, left:5,  delay:twnDelay})

}

function seq04(){
	console.log("seq04");

	var twnDelay=0;

	// hide seq 1
	TweenLite.to($('.q7'), 1, {autoAlpha:0})
	TweenLite.to($('.q7Txt'), 1, {autoAlpha:0})
	TweenLite.to($('.a6'), 1, {autoAlpha:0})
	TweenLite.to($('.a6Txt'), 1, {autoAlpha:0})
	TweenLite.to($('.s8'), 1, {autoAlpha:0})
	TweenLite.to($('.s8Txt'), 1, {autoAlpha:0})
	TweenLite.to($('.choose1'), 1, {autoAlpha:0})
	TweenLite.to($('.choose2'), 1, {autoAlpha:0})
	// hide seq 1

	twnDelay += 0.5;

	TweenLite.to($('.s8-2'), 1, {top:3, delay:twnDelay})
	TweenLite.to($('.s8Txt-2'), 1, {top:1, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.premium11'), 1, {top:4, delay:twnDelay})
	TweenLite.to($('.premiumPrice11'), 1, {top:27, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.pplus11'), 1, {top:5, delay:twnDelay})
	TweenLite.to($('.ppPrice11'), 1, {top:27, delay:twnDelay})

	twnDelay += 0.5;

	TweenLite.to($('.prestige11'), 1, {top:34, delay:twnDelay})
	TweenLite.to($('.prestigePrice11'), 1, {top:57, delay:twnDelay})

	twnDelay += 0.5;
	TweenLite.delayedCall(twnDelay, doResolve);
}
	


function doResolve(){
	console.log("doResolve");

	var twnDelay=0;
	
	$('.replayBtn').show();
	TweenLite.to($('.replayBtn img'), 1, {top: 49, delay:twnDelay})
	
	twnDelay+=1;
	TweenLite.delayedCall(twnDelay, returnTimer);

}

//A simple helper function to do display:none to multiple items
function hideAll(whichOnes){
	for (q=0;q<whichOnes.length;q++){
		$(whichOnes[q]).hide();
	}
}

//This funciton should be called when someone clicks on the unit or any cta
function clickThrough(){
	console.log("clickThrough");
	$('.q7').click(function(){
		var twnDelay = 0;
		twnDelay += 0;
		TweenLite.delayedCall(twnDelay, seq02);
	});
	$('.q7Txt').click(function(){
		var twnDelay = 0;
		twnDelay += 0;
		TweenLite.delayedCall(twnDelay, seq02);
	});

	$('.a6').click(function(){
		var twnDelay = 0;
		twnDelay += 0;
		TweenLite.delayedCall(twnDelay, seq03);
	});
	$('.a6Txt').click(function(){
		var twnDelay = 0;
		twnDelay += 0;
		TweenLite.delayedCall(twnDelay, seq03);
	});

	$('.s8').click(function(){
		var twnDelay = 0;
		twnDelay += 0;
		TweenLite.delayedCall(twnDelay, seq04);
	});
	$('.s8Txt').click(function(){
		var twnDelay = 0;
		twnDelay += 0;
		TweenLite.delayedCall(twnDelay, seq04);
	});

	// Audi A6

	$('.forward').click(function(){
		var twnDelay = 0;
		TweenLite.to($('.forward'), 1, {left:-207})
		TweenLite.to($('.back'), 1, {top:58, left:8, delay:twnDelay})
		TweenLite.to($('.a6-2'), 1, {top:400})
		twnDelay += 0.5;
		TweenLite.to($('.a6-p'), 1, {top: 4, delay:twnDelay} )
	})

	$('.back').click(function(){
		var twnDelay = 0;
		TweenLite.to($('.forward'), 1, {top:56, left:8})
		TweenLite.to($('.back'), 1, {left:-207, delay:twnDelay})
		TweenLite.to($('.a6-p'), 1, {top:400})
		TweenLite.to($('.a6-2'), 1, {top: 3})
	})

	// Audi A6

	// Audi Q7
	$('.start').click(function(){
		var twnDelay = 0;
		//hide
		TweenLite.to($('.q7-2'), 1, {autoAlpha:0})
		TweenLite.to($('.q7Txt'), 1, {autoAlpha:0})
		TweenLite.to($('.premium'), 1, {autoAlpha:0})
		TweenLite.to($('.pplus'), 1, {autoAlpha:0})
		TweenLite.to($('.prestige'), 1, {autoAlpha:0})
		TweenLite.to($('.ppPrice'), 1, {autoAlpha:0})
		TweenLite.to($('.prestigePrice'), 1, {autoAlpha:0})
		TweenLite.to($('.premiumPrice'), 1, {autoAlpha:0})
		TweenLite.to($('.start'), 1, {autoAlpha:0})
		//hide
		TweenLite.to($('.road'), 1, {top:0})
		TweenLite.to($('.q7r'), 1, {left:612})

		// Rotation 1
		twnDelay += 6;
		TweenLite.to($('.q7r'), 1, {rotation:50, top:15, delay:twnDelay})
		twnDelay += 0.1;
		TweenLite.to($('.q7r'), 1, {top:-45, left: 580, delay:twnDelay})
		twnDelay += 0.2;
		TweenLite.to($('.q7r'), 1, {rotation: 0, left: 580, delay:twnDelay})
		twnDelay += 2;
		TweenLite.to($('.q7r'), 1, {rotation:-50, top:35, delay:twnDelay})
		twnDelay += 0.5
		TweenLite.to($('.q7r'), 1, {rotation:0 , delay:twnDelay})
		// Rotation 1

		// Rotation 2
		twnDelay += 18;
		TweenLite.to($('.q7r'), 1, {rotation:50, top:15, delay:twnDelay})
		twnDelay += 0.1;
		TweenLite.to($('.q7r'), 1, {top:-45, left: 560, delay:twnDelay})
		twnDelay += 2;
		TweenLite.to($('.q7r'), 1, {rotation:-50, top:35, delay:twnDelay})
		twnDelay += 0.5
		TweenLite.to($('.q7r'), 1, {rotation:0, left: 540 , delay:twnDelay})
		// Rotation 2
		twnDelay += 0.5;
		TweenLite.to($('.sea'), 1, {autoAlpha:1, delay:twnDelay});
		twnDelay +=0.8;
		TweenLite.to($('.sea'), 1, {autoAlpha:0, delay:twnDelay});
		twnDelay += 0.2
		TweenLite.to($('.q7r'), 10, {left:70, delay:twnDelay})
		TweenLite.to($('.road'), 40, {left:-20})
	})
	// Audi Q7
}

//Replay the ad
function replay(){
	TweenLite.killTweensOf($(".container").find('*'));
	resetAll();
	setInitialStates();
	seq01();
}

//This resets everything in the container div to its original state as mandated by the css file
function resetAll(){
	TweenLite.set($(".container").find('*'), {clearProps:"all"});
}

//This is where we add any rollovers or clicks
function addListeners(){
	//ClickScreen
	$(".clickScreen").click(function(){
		clickThrough();
	});

	//cta
	$(".ctaContainer").click(function(){
		clickThrough();
	});

	$(".ctaContainer").on("mouseover",
		function(){
			TweenLite.to($(".ctaBg"), 0.3, {scale:1.1, ease:Quad.easeInOut, force3D:false});
		}
	);
	$(".ctaContainer").on("mouseout",
		function(){
			TweenLite.to($(".ctaBg"), 0.3, {scale:1, ease:Quad.easeInOut });
		}
	);

	//replay button
	$(".replayBtn img").click(function(){
		replay();
	});
	$(".replayBtn").on("mouseover",
		function(){
			TweenLite.to($(".replayBtn img"), 0.5, {rotation:-360, transformOrigin:"50% 57%", overwrite:true});
		}
	);
	$(".replayBtn").on("mouseout",
		function(){
			TweenLite.set($(".replayBtn img"), {rotation:0, overwrite:true});
		}
	);
	// Audi S8 Lights on hover
	$('.premium11').on('mouseover', function(){
		TweenLite.to($('.left'), 1, {autoAlpha:1})
		TweenLite.to($('.right'), 1, {autoAlpha:1})
	})

	$('.pplus11').on('mouseover', function(){
		TweenLite.to($('.left'), 1, {autoAlpha:1})
		TweenLite.to($('.right'), 1, {autoAlpha:1})
	})

	$('.prestige11').on('mouseover', function(){
		TweenLite.to($('.left'), 1, {autoAlpha:1})
		TweenLite.to($('.right'), 1, {autoAlpha:1})
	})


	$('.prestige11').on('mouseout', function(){
		TweenLite.to($('.left'), 1, {autoAlpha:0})
		TweenLite.to($('.right'), 1, {autoAlpha:0})
	})

	$('.pplus11').on('mouseout', function(){
		TweenLite.to($('.left'), 1, {autoAlpha:0})
		TweenLite.to($('.right'), 1, {autoAlpha:0})
	})

	$('.premium11').on('mouseout', function(){
		TweenLite.to($('.left'), 1, {autoAlpha:0})
		TweenLite.to($('.right'), 1, {autoAlpha:0})
	})
	// Audi S8 Lights on hover
}
//This will echo how many seconds have passed
function returnTimer(){
	stopWatch = ((new Date().getTime())-stopWatch) * 0.001;
	console.log(stopWatch+" seconds");
}